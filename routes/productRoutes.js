const express = require("express");
const router = express.Router();
const productControllers =  require("../controllers/productControllers.js");

const auth = require("../auth.js");



router.post("/create", auth.verify, (request, response) => 
{
		const result = {
			product: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
	productControllers.addProduct(request.body, result).then(resultFromController => response.send(resultFromController));
});





// // Idea from Sir John Marcelli Habab
// /* router.post("/create",auth.verify, courseControllers.addCourse) */

// // Get all courses
// router.get("/all", (request, response) => {
// 	productControllers.getAllProduct().then(resultFromController => response.send(resultFromController))
// });

// // [Additional Example]
// // [Arrow function to regular function]
// // Get ALL course in Regular function
// router.get("/getAllProductv2", (request, response) => {
// 	productControllers.getAllCourseInRegularFunction()
// 	.then(
// 		function getResultFromController(resultFromController) { 
// 			response.send(resultFromController)
// 		}
// 	);
// });


// // Get all ACTIVE courses
// router.get("/active", (request, response) => {
// 	productControllers.getActiveProduct().then(resultFromController => response.send(resultFromController))
// });


// // Get SPECIFIC course
// router.get("/:productId", (request, response) => {
// 	productControllers.getProduct(request.params.productId).then(resultFromController => response.send(resultFromController));
// })

// // [Additional Example]
// // [Arrow function to regular function]
// // Get SPECIFIC course
// router.get("/:productId/getSpecificV2", (request, response) => {
// 	productControllers.GetSpecificProductInRegularFunction(request.params.courseId)
// 	.then(
// 		function getResultFromController(resultFromController){
// 		 	response.send(resultFromController)
// 		}
// 	);
// })

// router.patch("/:productId/update", auth.verify, (request,response) => 
// {
// 	const newData = {
// 		product: request.body,  //request.headers.authorization contains jwt
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin 
// 	}
// 	productControllers.updateproduct(request.params.productId, newData).then(resultFromController => {
// 		response.send(resultFromController)
// 	})
// })


// // Archiving a single course
// /*
// 	router.patch("/:courseId/archive", auth.verify, (req, res) => {
// 		courseControllers.archiveCourse(req.params.courseId).then(resultFromController => {
// 			res.send(resultFromController)
// 		})
// 	})
// */


// router.patch("/:productId/archive", auth.verify, (request,response) => 
// {
// 	const productId = request.params.productId;
//     const newData = {
// 		product: request.body, 	//request.headers.authorization contains jwt
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}

// 	productControllers.archiveProduct(request.params.productId, newData).then(resultFromController => {
// 		response.send(resultFromController)
// 	})
// })









module.exports = router;