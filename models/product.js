// dependency
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Product is required"]
		},

		description: {
			type: String,
			required: [true, "Description is required"]
		},

		price : {
			type: Number,
			required: [true, "Price is required"]
		},

		isActive:{
			type: Boolean,
			default: true
		},
		Available:{
			type: Number,
			required: [true, "Available is required"]
		},
		createdOn : {
			type: Date,
			// The "new Date()" expression instantiates a new "Date" that the current date and time whenever a course is created in our database
			default: new Date()
		},

		SureBuyers : [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},
				DateBy: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
	
) 

module.exports = mongoose.model("Product", productSchema);



/*

"name" : "Infomation Technology",
"enrollees" : [
	{
		"userId" : "001",
		"enrolledOn" : "Feb 15, 2022"	
	},
	{
		"userId" : "002",
		"enrolledOn" : "Feb 15, 2022"	
	},
	{
		"userId" : "003",
		"enrolledOn" : "Feb 15, 2022"	
	}
]

*/