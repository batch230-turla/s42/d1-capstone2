// s40

const mongoose = require("mongoose");
const Course =  require("../models/course.js");

// S39 Activity - Controller
// Function for adding a course
// 2. Update the "addCourse" controller method to implement "admin authentication" for creating a course.
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newCourse.save().then((newCourse, error) =>
	{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}*/

// s39 solution
module.exports.addCourse = (reqBody, result) => {
	if(result.isAdmin == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((newCourse, error) =>
		{
			if (error){
				return error;		
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


//  Idea from Sir John Marcelli Habab

/*module.exports.addCourse = (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		})
		newCourse.save()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	}else{
		let message = 'User does not have Admin privileges';
		return res.send(message);
	}*/

// GET ALL course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}


// [Additional Example]
// [Arrow function to regular function]
// Get ALL course feature converted to regular function
// 'getAllCourseInRegularFunction' acts as a variable/storage of getAllCoursesV2() function so it could be exported
module.exports.getAllCourseInRegularFunction = 
function getAllCoursesV2(){
	return Course.find({}).
	then(
			function sendResult(result)
			{
				return result;
			}
	);
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}


// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}


// GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result =>{
		return result;
	})
}

// [Additional Example]
// [Arrow function to regular function]
// Get ALL course feature converted to regular function
// 'GetSpecificCourseInRegularFunction' acts as a variable/storage of getCourseV2() function so it could be exported
module.exports.GetSpecificCourseInRegularFunction = 
function getCourseV2(courseId){
	return Course.findById(courseId).
	then(
		function sendResult(result){
			return result;
		}
	);
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	
	else{
		// Promise.resolve() is required if it expects a solved promise

		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {
			console.log(value)
			return value
		});
		// Compared to regular return it will not work
		// return 'User must be ADMIN to access this functionality';
	}
}


// Soft delete happens when a course status (isActive) is set to false.
/*
	module.exports.archiveCourse = (courseId) => {
		return Course.findByIdAndUpdate(courseId, {
			isActive: false
		})
		.then((archivedCourse, error) => {
			if(error){
				return false
			} 

			return true
		})
	}
*/

// S40 Activity Solutions - Archive Course with restriction for non-admin tokens

// Solution 1 - Auto update, no need an input from the body
module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
				{ isActive: false }
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// Solution 2 - 
// Needs and input from the body
/*
	module.exports.archiveCourse = (courseId, newData) => {
		if(newData.isAdmin == true){
			return Course.findByIdAndUpdate(courseId,
					{ isActive: newData.course.isActive }
			).then((result, error)=>{
				if(error){
					return false;
				}
				return true
			})
		}
		else{
			let message = Promise.resolve('User must be ADMIN to access this functionality');
			return message.then((value) => {return value});
		}
	}
*/




