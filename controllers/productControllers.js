

const mongoose = require("mongoose");
const Product =  require("../models/product.js");



module.exports.addProduct = (reqBody, result) => {
	if(result.isAdmin == true){
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newProduct.save().then((newProduct, error) =>
		{
			if (error){
				return error;		
			}
			else{
				return newProduct;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}



// module.exports.getAllProduct = () => {
// 	return Product.find({}).then(result =>{
// 		return result;
// 	})
// }



// module.exports.getAllProductInRegularFunction = 
// function getAllProductV2(){
// 	return Product.find({}).
// 	then(
// 			function sendResult(result)
// 			{
// 				return result;
// 			}
// 	);
// }


// module.exports.getActiveProduct = () => {
// 	return Product.find({isActive:true}).then(result =>{
// 		return result;
// 	})
// }



// module.exports.getActiveProduct = () => {
// 	return Product.find({isActive:true}).then(result =>{
// 		return result;
// 	})
// }


// module.exports.getProduct = (productId) => {
// 	return Product.findById(productId).then(result =>{
// 		return result;
// 	})
// }


// module.exports.GetSpecificProductInRegularFunction = 
// function getProductV2(productId){
// 	return Product.findById(productId).
// 	then(
// 		function sendResult(result){
// 			return result;
// 		}
// 	);
// }


// // UPDATING a course
// module.exports.updateProduct = (productId, newData) => {
// 	if(newData.isAdmin == true){
// 		return Product.findByIdAndUpdate(productId,
// 			{
// 				// newData.course.name
// 				// newData.request.body.name
// 				name: newData.product.name, 
// 				description: newData.product.description,
// 				price: newData.product.price
// 			}
// 		).then((result, error)=>{
// 			if(error){
// 				return false;
// 			}
// 			return true
// 		})
// 	}
	
// 	else{
// 		// Promise.resolve() is required if it expects a solved promise

// 		let message = Promise.resolve('User must be ADMIN to access this functionality');
// 		return message.then((value) => {
// 			console.log(value)
// 			return value
// 		});
// 		// Compared to regular return it will not work
// 		// return 'User must be ADMIN to access this functionality';
// 	}
// }


// // Soft delete happens when a course status (isActive) is set to false.
// /*
// 	module.exports.archiveCourse = (courseId) => {
// 		return Course.findByIdAndUpdate(courseId, {
// 			isActive: false
// 		})
// 		.then((archivedCourse, error) => {
// 			if(error){
// 				return false
// 			} 

// 			return true
// 		})
// 	}
// */

// // S40 Activity Solutions - Archive Course with restriction for non-admin tokens

// // Solution 1 - Auto update, no need an input from the body
// module.exports.archiveProduct = (productId, newData) => {
// 	if(newData.isAdmin == true){
// 		return Product.findByIdAndUpdate(productId,
// 				{ isActive: false }
// 		).then((result, error)=>{
// 			if(error){
// 				return false;
// 			}
// 			return true
// 		})
// 	}
// 	else{
// 		let message = Promise.resolve('User must be ADMIN to access this functionality');
// 		return message.then((value) => {return value});
// 	}
// }


// Solution 2 - 
// Needs and input from the body
/*
	module.exports.archiveCourse = (courseId, newData) => {
		if(newData.isAdmin == true){
			return Course.findByIdAndUpdate(courseId,
					{ isActive: newData.course.isActive }
			).then((result, error)=>{
				if(error){
					return false;
				}
				return true
			})
		}
		else{
			let message = Promise.resolve('User must be ADMIN to access this functionality');
			return message.then((value) => {return value});
		}
	}
*/




