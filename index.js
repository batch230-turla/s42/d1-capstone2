/*
	Gitbash:
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
*/

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// 1
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


const app = express();


app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended:true}));


app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
app.use("/product", productRoutes);


mongoose.connect("mongodb+srv://admin:admin@batch230.d2wmu0h.mongodb.net/s42-Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Turla-Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () => 
	{ console.log(`API is now online on port ${process.env.PORT || 4000} `)
});



